const Category = require('../models/category')


exports.postCategory= async (req,res)=>{
   try {
       const category = await Category.create(req.body);
       res.status(201).json({success:true,data:category})
   }catch (error) {
       return res.status(404).json(error)
   }
}

exports.getCategory = async (req,res)=>{
    const categories = await Category.find()
    if(!Object.keys(categories).length){
        return res.status(404).json({data:'There are no categories'})
    }
    res.status(200).json({success:true,data:categories})
}
exports.getSingleCategory = async (req,res)=>{
    try {
        const category = await Category.findById(req.params.id)
        if(!category){
            return res.status(404).json('There is no such category')
        }
        res.status(200).json({success:true,data:category})


    }catch (error){
        return res.status(404).json(error)
    }
}

exports.deleteCategory = async (req,res)=>{
    try {
        const category= await Category.findByIdAndDelete(req.params.id);
        if(!category){
            return res.status(404).json('There is no such category')
        }
        res.status(200).json({success:true,data:{message:'removed'}})
    }catch (error){
        return res.status(404).json(error)
    }

}
exports.updateCategory = async (req,res)=>{
    try {
        const newCategory = await Category.findByIdAndUpdate(req.params.id,req.body,{new :true,runValidators:true})
        if(!newCategory){
            return res.status(404).json('There is no such category')
        }
        res.status(200).json({success:true,data:newCategory})
    }catch (error){
        return res.status(404).json(error)
    }
}