const Order = require("../models/order");
const OrderITem = require('../models/order-item')
const mongoose = require("mongoose");
const Category = require("../models/category");
const Product = require("../models/product");

exports.getOrders= async (req,res)=>{
    const userList= await Order.find().populate('user','name').sort('-dateOrdered');
    if(!Object.keys(userList).length){
        return res.status(500).json('There are no orders')
    }
    res.send(userList)
}

exports.getSingleOrder= async (req,res)=> {
    if (!mongoose.isValidObjectId(req.params.id)) {
        return res.status(400).json({data: 'invalid params id'})
    }
    try {
        const order = await Order.findById(req.params.id).populate('user','name')
            .populate({path:'orderItem',populate:{path:'product',populate:'category'}})
        if (!order) {
            return res.status(400).json({data: 'There is no such order'})
        }
        res.send(order)
    } catch (error) {
        return res.status(404).json(error)

    }
}

exports.createOrder = async (req,res)=>{
    try {
        const orderItemsIds=Promise.all(req.body.orderItem.map(async orderItem=>{
            const newOrderItem={
                quantity:orderItem.quantity,
                product:orderItem.product
            }
            const orderItem2= await OrderITem.create({...newOrderItem})
            return orderItem2['_id'];
        }))
        const orderItemsResolved= await orderItemsIds;
        const totalPrices= await Promise.all(orderItemsResolved.map(async (orderItemsIds)=>{
            const orderItem= await OrderITem.findById(orderItemsIds).populate('product','price');
            const totalPrice= orderItem['product']['price'] * orderItem['quantity']
            return totalPrice
        }))
        const totalPrice= totalPrices.reduce((a,b)=>a+b,0)
        const orderData ={
            orderItem:orderItemsResolved,
            shippingAddress1:req.body.shippingAddress1,
            shippingAddress2:req.body.shippingAddress2,
            city:req.body.city,
            country:req.body.country,
            phone:req.body.phone,
            status:req.body.status,
            totalPrice:totalPrice,
            user:req.body.user,
            zip:req.body.zip
        }
        const order = await Order.create({...orderData})
        res.status(201).send(order)
    }catch (error){
        return res.status(404).json(error)

    }

}

exports.updateOrder= async (req,res)=>{
    const order= await Order.findByIdAndUpdate(
        req.params.id,
        {
        status: req.body.status
         },
        {new:true}
    )
    if(!order){
        return res.status(400).send('order not found')
    }
    res.send(order)
}
exports.deleteOrder = async (req,res)=>{
    try {
        const order= await Order.findByIdAndDelete(req.params.id);
        if(!order){
            return res.status(404).json('There is no such order')
        }
        order['orderItem'].map(async orderItem=>{
            const items= await OrderITem.findByIdAndRemove(orderItem)
            if(!items){
                return res.send('orderItem does not found')
            }
            return items
        })
        res.status(200).json({success:true,data:{message:'removed'}})
    }catch (error){
        return res.status(404).json(error)
    }
}

exports.totalSales = async (req,res)=>{
    const totalSales= await Order.aggregate([
        {$group:{_id:null, totalSales:{$sum:'$totalPrice'}}}
    ])
    if(!totalSales){
        return res.status(400).send('the order sales cannot be generated')
    }
    res.send({totalSales:totalSales})
}

exports.orderCount= async (req,res)=>{
    try {
        const orderCount= await Order.count()
        if(!orderCount){
            return '0'
        }
        res.send({
            count:orderCount
        })
    }catch (error){
        return res.status(404).json(error)
    }
}

exports.getCurrentUserOrders= async (req,res)=>{
    const orderList= await Order.find({user:req.params.id}).populate({path:'orderItem',populate:{path:'product',populate:'category'}})
    if(!Object.keys(orderList).length){
        return res.status(500).json('There are no orders')
    }
    res.send(orderList)
}