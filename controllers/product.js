const Product= require('../models/product')
const Category = require('../models/category')
const mongoose = require('mongoose')


exports.getProducts = async (req,res)=>{
   try {
       let filter={}
       if(req.query.categories){
            filter = {category:req.query.categories.split(',')}
       }
       const products = await Product.find(filter).populate('category')
       if(!Object.keys(products).length){
           return res.status(404).json({data:'There are no any more products'})
       }
       res.status(200).json({success:true,data:products})
   }catch (err){
       return res.status(404).json(err)

   }
}

exports.postProduct= async (req,res)=>{
   try {
       const category = await Category.findById(req.body.category)
       if(!category){
           return res.status(400).json({data:'There is no such category'})
       }
       const product = await Product.create(req.body);
       res.status(201).json({success:true,data:product})
   }catch (error){
       return res.status(404).json(error)
   }
}
exports.getSingleProduct= async (req,res)=>{
    if(! mongoose.isValidObjectId(req.params.id)){
        return  res.status(400).json({data:'invalid params id'})
    }
    try {
        const product = await Product.findById(req.params.id).select('name image -_id').populate('category');
        if(!product){
            return res.status(400).json({data:'There is no such product'})
        }
        res.send(product)
    }catch (error){
        return res.status(404).json(error)

    }

}

exports.deleteProduct= async (req,res)=>{
    if(! mongoose.isValidObjectId(req.params.id)){
        return  res.status(400).json({data:'invalid params id'})
    }
    try {
        const product = await Product.findByIdAndDelete(req.params.id);
        if(!product){
            return res.status(400).json({data:'There is no such product'})
        }
        res.status(200).json({success:true,data:{message:'removed'}})
    }catch (err){
        return res.status(404).json(err)
    }
}
exports.updateProduct = async (req,res)=>{
    if(! mongoose.isValidObjectId(req.params.id)){
        return  res.status(400).json({data:'invalid params id'})
    }
    try {
        const newProduct = await Product.findByIdAndUpdate(req.params.id,req.body,{new :true,runValidators:true})
        if(!newProduct){
            return res.status(404).json('There is no such product')
        }
        const category = await Category.findById(req.body.category)
        if(!category){
            return res.status(400).json({data:'There is no such category'})
        }
        res.status(200).json({success:true,data:newProduct})
    }catch (error){
        return res.status(404).json(error)
    }
}

exports.productCount= async (req,res)=>{
    try {
        const productCount= await Product.count()
        if(!productCount){
            return '0'
        }
        res.send({
            count:productCount
        })
    }catch (error){
        return res.status(404).json(error)
    }
}

exports.featuredProducts= async (req,res)=>{
    try {
        const count= req.params.count ? req.params.count:0;
        const featuredProducts= await Product.find({isFeatured:true}).limit(count)
        if(!featuredProducts){
            return res.status(400).json({data:'There is no such products'})
        }
        res.send(featuredProducts)
    }catch (error){
        return res.status(404).json(error)
    }
}
