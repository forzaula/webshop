const User= require('../models/user');
const bcrypt=require('bcryptjs');
const mongoose = require("mongoose");
const jwt= require('jsonwebtoken');

exports.getUsers= async (req,res)=>{
    const userList= await User.find().select('-passwordHash');
    if(!Object.keys(userList).length){
         return res.status(500).json('There are no users')
    }
    res.send(userList)
}

exports.getSingleUser= async (req,res)=>{
    if(! mongoose.isValidObjectId(req.params.id)){
        return  res.status(400).json({data:'invalid params id'})
    }
    try {
        const user = await User.findById(req.params.id).select('-passwordHash')
        if(!user){
            return res.status(400).json({data:'There is no such user'})
        }
        res.send(user)
    }catch (error){
        return res.status(404).json(error)

    }
}

exports.createUser = async (req,res)=>{
    try {
        const candidate = await User.find({email:req.body.email})
        if(Object.keys(candidate).length){
            return res.send('такой пользователь уже есть')
        }
        const hashPassword = bcrypt.hashSync(req.body.password, 7)
        const userData = {
            name:req.body.name,
            email:req.body.email,
            passwordHash:hashPassword,
            phone:req.body.phone,
            isAdmin:req.body.isAdmin,
            street:req.body.street,
            apartment:req.body.apartment,
            zip:req.body.zip,
            city:req.body.city,
            country:req.body.country
        }
        const user = await User.create({...userData})
        res.status(201).json({success:true,data:user})
    }catch (error) {
        return res.status(404).json(error)
    }
}

exports.loginUser= async (req,res)=>{
    const {email, password} = req.body
    const secret= process.env.SECRET
    const user = await User.findOne({email})
    if (!user) {
        return res.status(400).json({message: `${email} does not found`})
    }
    const validPassword = bcrypt.compareSync(password, user["passwordHash"])
    if(validPassword){
        const token= await jwt.sign({
            userId: user["id"],
            isAdmin: user['isAdmin']
        },
            secret,
            {expiresIn: '1d'})
        res.status(200).send({user:user['email'],token:token})
    }else {
        res.status(400).send('Password is wrong')
    }
}
exports.userCount= async (req,res)=>{
    try {
        const userCount= await User.count()
        if(!userCount){
            return '0'
        }
        res.send({
            count:userCount
        })
    }catch (error){
        return res.status(404).json(error)
    }
}

exports.deleteUser= async (req,res)=>{
    if(! mongoose.isValidObjectId(req.params.id)){
        return  res.status(400).json({data:'invalid params id'})
    }
    try {
        const user = await User.findByIdAndDelete(req.params.id);
        if(!user){
            return res.status(400).json({data:'There is no such user'})
        }
        res.status(200).json({success:true,data:{message:'removed'}})
    }catch (err){
        return res.status(404).json(err)
    }
}
