const express = require('express');
const router = express.Router();
const {
    getProducts,
    postProduct,
    getSingleProduct,
    deleteProduct,
    updateProduct,
    productCount,
    featuredProducts} = require('../controllers/product')

router.route('/')
    .get(getProducts)
    .post(postProduct);

router.route('/:id')
    .get(getSingleProduct)
    .delete(deleteProduct)
    .put(updateProduct);

router.route('/get/count').get(productCount);

router.route('/get/featured/:count').get(featuredProducts);

module.exports= router;