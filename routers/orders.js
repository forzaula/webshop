const express = require('express');
const router = express.Router();
const {getOrders,
    createOrder,
    getSingleOrder,
    updateOrder,
    deleteOrder,
    totalSales,
    orderCount,
    getCurrentUserOrders} = require('../controllers/order')

router.route('/')
    .get(getOrders)
    .post(createOrder)

router.route('/:id')
    .get(getSingleOrder)
    .put(updateOrder)
    .delete(deleteOrder)

router.route('/get/totalsales').get(totalSales)

router.route('/get/count').get(orderCount)

router.route('/get/userorders/:id').get(getCurrentUserOrders)

module.exports= router;