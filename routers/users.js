const express = require('express');
const router = express.Router();
const {getUsers,
    createUser,
    getSingleUser,
    loginUser,
    userCount,
    deleteUser}= require('../controllers/user')

router.route('/')
    .get(getUsers)
    .post(createUser);

router.route('/register').post(createUser);

router.route('/:id')
    .get(getSingleUser)
    .delete(deleteUser);

router.route('/login').post(loginUser)

router.route('/get/count').get(userCount)

module.exports= router;