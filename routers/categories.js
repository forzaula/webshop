const express = require('express');
const router = express.Router();
const {
    postCategory,
    getCategory,
    deleteCategory,
    getSingleCategory,
    updateCategory}= require('../controllers/category')

router.route('/')
    .post(postCategory)
    .get(getCategory)

router.route('/:id')
    .delete(deleteCategory)
    .get(getSingleCategory)
    .put(updateCategory)

module.exports= router;