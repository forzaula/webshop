const expressJwt= require('express-jwt');

function authJwt(){
    const api= process.env.API_URL
    const secret = process.env.SECRET
    return expressJwt({
        secret,
        algorithms:['HS256'],
        isRevorked:isRevorked
    }).unless({
        path:[
            {url: /\/api\/v1\/products(.*)/,methods:['GET','OPTIONS']},
            {url: /\/api\/v1\/categories(.*)/,methods:['GET','OPTIONS']},
            `${api}/users/login`,
            `${api}/users/register`,
        ]
    })


}
async function isRevorked(req,payload,done){
    if(!payload.isAdmin){
        done(null,true)
    }
    done();
}


module.exports= authJwt;