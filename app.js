
//installed apps
const express =require('express');
const app = express();
const bodyParser = require('body-parser');
const morgan=require('morgan');
const mongoose = require('mongoose');
const cors = require('cors');
app.use(cors());
app.options('*',cors());



//  import from folders

require('dotenv/config');
const api= process.env.API_URL
const productsRouter= require('./routers/product')
const ordersRouter= require('./routers/orders')
const categoriesRouter= require('./routers/categories')
const usersRouter= require('./routers/users')
const authJwt= require('./helpers/jwt')
const errorHandler = require('./helpers/error-handler');

//middleware

app.use(bodyParser.json());
app.use(morgan('tiny'));
app.use(authJwt());
app.use(errorHandler)



//Routers
app.use(`${api}/products`,productsRouter);
app.use(`${api}/orders`,ordersRouter);
app.use(`${api}/categories`,categoriesRouter);
app.use(`${api}/users`,usersRouter);


mongoose.connect(process.env.CONNECTION_STRING).then(()=>{
    console.log('Database connection is ready')
}).catch((error)=>{
    console.log(error)
})

app.listen(3000,()=>{
    console.log("server has started on http://localhost:3000");
})